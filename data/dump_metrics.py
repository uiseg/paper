import json
from itertools import product
from argparse import ArgumentParser

from pandas import Series, MultiIndex

SEGMENT_LABEL = ['perfect', 'good', 'fair', 'bad']
CLUSTER_LABEL = [
    'perfect cluster', 'missing children', 'additional children', 'bad cluster'
]


def main(path):
    with open(path) as f:
        data = json.load(f)

    LABELS = {(a, b): 0
              for a, b in product(SEGMENT_LABEL, CLUSTER_LABEL)}
    for a, b in data:
        k = (a, b) if (a, b) in LABELS else (b, a)
        LABELS[k] += 1

    s = Series(LABELS, index=MultiIndex.from_tuples(LABELS))
    df = s.unstack()
    df = df[::-1][CLUSTER_LABEL] * 5 / 100

    total_num = df.sum().sum()

    df.loc['fair'] += df.loc['good']
    df.drop(['good'], inplace=True)
    SEGMENT_LABEL.remove('good')

    df.loc['total'] = df.sum()
    SEGMENT_LABEL.append('total')

    df['total'] = df.sum(axis=1)

    for seg in SEGMENT_LABEL:
        text = seg
        for num in zip(df.loc[seg]):
            num = num[0]
            percent = num / total_num * 100

            text += f' & {num:.1f} \percent{{{percent:.1f}}}'

        print(text + '\\\\')


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('input', help='Input path', default='data.json')

    args = parser.parse_args()

    main(args.input)
