Vision Based User Interface Segmentation Algorithm
==================================================

By Ian Chen and Sheng-De Wang at National Taiwan University

This is the repo for the paper, which is available [here](https://gitlab.com/uiseg/paper/-/jobs/artifacts/master/raw/main.pdf?job=build).
