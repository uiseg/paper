\subsection{Distance Model}

The regions detected in the last step are considered `content' elements coming
in a spatially flat structure, and the distance model define pairwise distance
between any two of these elements plus clusters of them.  It consists of two
metrics: the base distance that is calculated by directly using the dimension
information of regions, and the amplification factor that magifies the base
distance. We believe that this mechanism resembles how humans perceive user
interface. To put it more precisely, we observe that the distance model should
have the following characteristics:

\begin{enumerate}
    \item $\operatorname{distance}(a, b) > 0, \forall a, b$
    \item $\operatorname{distance}(a, b) = \operatorname{distance}(b, a)$
    \item $\operatorname{distance}(a, b') > \operatorname{distance}(a, b)$
        if there are lines between $a$ and $b$ but there is not between $a$
        and $b'$.
    \item $\operatorname{distance}(a, b) > \operatorname{distance}(a, c)$
        if $a$, $b$, $c$ are arraged spatially in the order
        $a \leftrightarrow c \leftrightarrow b$.
\end{enumerate}

Another important observation about distance calculation is that, we can ignore
the raw pixel information of the original user interface, as opposed to how
other vision-based segmentation algorithms work, and only consider dimension
information about the target elements in the distance calculation process. In
other words, we try to detect all needed features in the segmentation step and
process them in the clustering step; if we find the overall accuracy
unsatisfactory, we can either detect more features by updating segmentation
algorithm, or fine-tune the clustering method.

Features we get in the clustering process are boxes, horizontal/vertical
lines, and regions storing in their own planes, which is defined below:

\begin{definition}

    A \textbf{plane} is a collection of regions or lines that can be accessed
    in a spatial way. Two regions don't overlap with each other if they are in
    the different planes.

\end{definition}

Among these features, the region plane is the core one in the proposed distance
model and is used to calculate the base distance metric.

\begin{definition}

    Let $a = (a_{x1}, a_{y1}, a_{x2}, a_{y2})$, $b = (b_{x1}, b_{y1}, b_{x2},
    b_{y2})$ be two regions in the same plane with center $(a_{xc}, a_{yc})$
    and $(b_{xc}, b_{yc})$, respectively.  We define \textbf{base distance}
    between $a$ and $b$ to be
    \begin{equation}
        bd = \max(\dfrac w {RI}, h),
    \end{equation} where \begin{equation*}
     \begin{cases}
         w = \max(|a_{xc} - b_{xc}| - \frac {(a_{x2} - a_{x1}) + (b_{x2}
         - b_{x1})} 2, 0)\\

         h = \max(|a_{yc} - b_{yc}| - \frac {(a_{y2} - a_{y1}) + (b_{y2}
         - b_{y1})} 2, 0),\\
        \end{cases}
    \end{equation*} $RI$ is the \textbf{row inclication value} representing
    the tendency to merge regions in the same row rather than in the same
    column.

\end{definition}

The proper row inclication value varies by user interface environments. We find
that $3$ is a proper value for a general use case.

Then, we introduce size difference and separator effect that compose
amplification factor. The size difference take into consideration
that we want to group lists of elements together, and they often comes in
similar sizes.

\begin{definition}

    The \textbf{size difference} of region $a$ and $b$ is \begin{equation}
    A_{\D{size}} = \dfrac 2 {1 + s_r},\end{equation} where \begin{equation*}
        s_r = \min(\frac
            {\operatorname{Area}(a)} {\operatorname{Area}(b)},
            \frac {\operatorname{Area}(b)} {\operatorname{Area}(a)}).
    \end{equation*}

\end{definition}

This function is chosen as it is a convex downward function passing through
points $(0, 2)$ and $(1, 1)$.

As for separator effect, we first define critical region between any two
regions; then, lines within the critical region will make merging the two
regions more difficult.

\begin{definition}

    Using the notaion discussed above, we define \textbf{critical region}
    $Q_{ab}$ to be the largest region within $G_{ab} - a - b$,
    where $G_{ab} = (g_{x1}, g_{y1}, g_{x2}, g_{y2})$,
    \begin{equation*}
        \begin{cases}
            g_{x1} = \min(a_{xc}, b_{xc})\\
            g_{y1} = \min(a_{yc}, b_{yc})\\
            g_{x2} = \max(a_{xc}, b_{xc})\\
            g_{y2} = \max(a_{yc}, b_{yc})
        \end{cases}
    \end{equation*}

\end{definition}

In Figure \ref{img:distance} we show an example with two regions and the
critical region of them.

\begin{definition}

    We define \textbf{separator effect value} to be

    \begin{equation}
        A_{\D{sep}} = \prod_{l \in L \cap Q_{ab}}
        {\dfrac {|l|} {\max(\dim(a, b), |l|)}},
    \end{equation} where $|l|$ denotes the length of line $l$, and
    \begin{equation*}
        \dim(a, b) = \begin{cases}
            \min(\operatorname{width}(a), \operatorname{width}(b)) &, \text{if}
            \operatorname{Axis}(l) \text{ is } W\\
            \min(\operatorname{height}(a), \operatorname{height}(b)) &,
            \text{otherwise}.
        \end{cases}
    \end{equation*}

\end{definition}

Finally, the pairwise distance between any two regions is defined below.

\begin{definition}

    The \textbf{pairwise distance} of any two regions $a$ and $b$ is defined as

    \begin{equation}
        \operatorname{distance}(a, b) = \begin{cases}
            bd ^ {A_{\D{sep}} \times A_{\D{size}}}&,
                \text{ $a, b$ are in the same box}\\
            \infty&, \text{otherwise}.
        \end{cases}
    \end{equation}
\end{definition}

The use of exponential function in our distance model may seem unintuitive
at first glance, but actually it is required for better clustering performance,
which we will discuss later.

\begin{figure}[t]
\centering
\begin{minipage}{.45\linewidth}
  \includegraphics[width=\linewidth]{images/distance1}
  \label{distance1}
\end{minipage}
\hspace{.05\linewidth}
\begin{minipage}{.45\linewidth}
  \includegraphics[width=\linewidth]{images/distance2}
  \label{distance2}
\end{minipage}
\caption{Left: The critical section and base distance of a and b.
Right: Line 1 has separator effect greater than Line 2.}
\label{img:distance}
\end{figure}
