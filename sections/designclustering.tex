\subsection{Hierarchical Clustering}

The goal of hierarchical clustering is to create a segment tree from the
elements detected in the segmentation step, with each node in the segment tree
a visually and semantically coherent segment. We find that by using the
distance model defined in the previous section, clustering can be done
iteratively with a simple threshold selection method.

\begin{algorithm}[b]

    \begin{algorithmic}[1]

        \renewcommand{\algorithmicrequire}{\textbf{Input:}}
        \renewcommand{\algorithmicensure}{\textbf{Output:}}

        \REQUIRE regions, boxes

        \ENSURE a segment tree of the user interface

        % \BlankLine

        \STATE Start from the smallest box to the biggest box


        \IF {there is only one region covered by the box}

            \STATE Remove the box. Go to Line 1 to process the next box.

        \ENDIF


        \STATE Add all regions covered by the box into a new plane. Select a clustering threshold.

        \WHILE {there are some region pairs with pairwise distance < threshold}

            \STATE Remove them from the plane.

            \STATE Merge connected components into clusters and add these clusters into the plane.

            \STATE Update pairwise distance information.

            \STATE Select a new clustering threshold.

        \ENDWHILE

        \STATE Remove regions in the plane and merge them into a cluster.

        \STATE Remove the box. Go to Line 1 to process the next box.

    \end{algorithmic}

    \caption{\uiseg{} Algorithm}

\end{algorithm}

By iterating from the smallest box to the biggest box, we can ensure that there
is not any box within candidates in each iteration. Then, all boxes will be
converted to clusters by the end of the algorithm.

The last thing about the clustering algorithm is the heuristic of determining
a good threshold in Line 5 of the algorithm. There is a trade-off about the
selection of threshold: if the threshold is too small, we only merge two
regions in each iteration, and the created cluster might not be a valid
coherent segment; on the other hand, if the threshold is too large, the final
segment tree would contain too few nodes, and the algorithm would become
useless.

The good news is that, because of the exponential function used to calculate
pairwise distance, the distribution of distance has a upward-sloping, concave
upward curve, and small distance tends to be grouped together. Therefore,
selecting a threshold becomes easy: we sort the distance sequence and compute
its discrete-time derivative.  The first index at which the derivative
increases is used to find the threshold in the original sequence.
